﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GoFish
{
    public partial class mainForm : Form
    {
        bool restartGame = false;

        public mainForm()
        {
            InitializeComponent();
        }

        private Game game;

        private void buttonStart_Click(object sender, EventArgs e)
        {
            if (!restartGame)
            {
                if (String.IsNullOrEmpty(textName.Text))
                {
                    MessageBox.Show("If you are not 404 - don't forget to enter your name :)", "You can't start game yet");
                    return;
                }
                restartGame = true;

                string botName1 = "Janusz";
                string botName2 = "Mietek";
                if (textName.Text == "Janusz")
                    botName1 = "Seba";
                else if (textName.Text == "Mietek")
                    botName2 = "Seba";

                game = new Game(textName.Text, new List<string> { botName1, botName2 }, textProgress);

                buttonStart.Enabled = false;
                textName.Enabled = false;
                buttonAsk.Enabled = true;

                UpdateForm();
            }
            else
            {
                Application.Restart();
            }
        }

        private void UpdateForm()
        {
            listHand.Items.Clear();
            foreach (String cardName in game.GetPlayerCardNames())
                listHand.Items.Add(cardName);

            textBooks.Text = game.DescribeBooks();
            textProgress.Text += game.DescribePlayerHands();
            textProgress.SelectionStart = textProgress.Text.Length;
            textProgress.ScrollToCaret();
        }

        private void buttonAsk_Click(object sender, EventArgs e)
        {
            PlayNextRound();
        }

        private void listHand_DoubleClick(object sender, EventArgs e)
        {
            PlayNextRound();
        }

        private void PlayNextRound()
        {
            if (listHand.SelectedIndex < 0)
            {
                MessageBox.Show("Select card");
                return;
            }

            textProgress.Text = "";

            if (game.PlayOneRound(listHand.SelectedIndex))
            {
                textProgress.Text += "The winner is... " + game.GetWinnerName();
                textBooks.Text = game.DescribeBooks();
                buttonAsk.Enabled = false;
                listHand.Enabled = false;

                buttonStart.Enabled = true;
                buttonStart.Text = "Restart game";
            }
            else
                UpdateForm();
        }
    }
}
