# GoFish!
.NET card game where you play versus 2 computers. Application use file from "CardRandomizer" and "Deck Organizer" projects.
Application is based on book "Head First C#" O'REILLY Jennifer Greene, Andrew Stellman
## Purpose
The purpose of this application is to learn more about IEnumerable<T> interface, Dictionary and enums.
## Rules
Five cards are dealt from a standard 52-card deck to each player. The remaining cards are shared between the players, spread out in a disorderly pile referred to as the "ocean" or "pool" - in application we call it stock.
The player whose turn it is to play asks another player for his or her cards of a particular face value. For example, Alice may ask, "Bob, do you have any threes?" Alice must have at least one card of the rank she requested. Bob must hand over all cards of that rank if possible. If he has none, Alice draws a card from the pool and places it in her own hand. Then it is the next player's turn. When any player at any time has all four cards of one face value, it forms a book, and the cards must be placed face up in front of that player.
The game end when there are any more cards in the stock. The player with the most books wins.
## Author
* **Patryk Kupis** - [Linkedin](https://www.linkedin.com/in/patryk-kupis-12a453162/), [Gitlab](https://gitlab.com/Kupis)
## License
This project is licensed under the MIT License - see the [LICENSE](LICENSE) file for details